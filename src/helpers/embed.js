const GIF = (gif) => {
  return {
    "embed": {
      "color": 30914,
      "image": {
        "url": gif
      }
    }
  };
};

const HELP = (message) => {
  return {
    "embed": {
      "title": 'List of Commands',
      "color": 4886754,
      "timestamp": Date(),
      "footer": {
        "text": 'Hellybot'
      },
      "fields": message
    }
  };
};

const HELP2 = (command) => {
  return {
    "embed": {
      "description": command.value,
      "color": 4886754,
      "timestamp": Date(),
      "footer": {
        "text": 'Hellybot'
      },
      "author": {
        "name": command.name
      }
    }
  };
}

const ERROR = (message) => {
  return {
    "embed": {
      "title": message,
      "color": 15466518
    }
  };
};

const SAVED_CHAR = (name, server, thumbnail, message) => {
  return {
    "embed": {
      "description": message,
      "color": 3068177,
      "timestamp": Date(),
      "footer": {
        "text": 'Hellybot'
      },
      "thumbnail": {
        "url": thumbnail
      },
      "author": {
        "name": name + ' of ' + server
      }
    }
  };
};

const CHAR_ERROR = (message) => {
  return {
    "embed": {
      "description": message,
      "color": 15466518
    }
  };
};

const PORTRAIT = (name, server, picture) => {
  return {
    "embed": {
      "color": 4886754,
      "timestamp": Date(),
      "footer": {
        "text": "Hellybot"
      },
      "image": {
        "url":  picture
      },
      "author": {
        "name": name + ' of ' + server,
      }
    }
  };
};

const CHAR_PROFILE = (details, portrait, thumbnail, name, server) => {
  return {
    "embed": {
      "description": details,
      "color": 3068177,
      "timestamp": Date(),
      "footer": {
        "text": "Hellybot"
      },
      "image": {
        "url": portrait
      },
      "author": {
        "icon_url": thumbnail,
        "name": name + ' of ' + server,
      }
    }
  };
};

module.exports = { 
  GIF,
  ERROR,
  HELP,
  HELP2,
  PORTRAIT,
  SAVED_CHAR,
  CHAR_ERROR,
  CHAR_PROFILE
};