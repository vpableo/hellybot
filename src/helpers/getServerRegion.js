const CONFIG = require('../../config');

const getServerRegion = (charServer) => {
  let serverRegion;
  for (region in CONFIG.SERVERS) {
    if (CONFIG.SERVERS[region].includes(charServer)) {
      serverRegion = region
      break;
    }
  }
  return serverRegion;
}

module.exports = getServerRegion;
