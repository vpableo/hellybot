const EMBED = require('./embed');
const CONFIG = require('../../config');

const defaultError = (receivedMessage) => {
  receivedMessage.clearReactions();
  receivedMessage.channel.send(EMBED.CHAR_ERROR(CONFIG.MESSAGES.somethingWrong));
}

const commandNotfound = (receivedMessage) => {
  receivedMessage.channel.send(EMBED.ERROR(CONFIG.MESSAGES.commandError));
}

const charNotfound = (receivedMessage) => {
  receivedMessage.clearReactions();
  receivedMessage.channel.send(EMBED.CHAR_ERROR(CONFIG.MESSAGES.characterNotFound));
};

const logsNotfound = (receivedMessage) => {
  receivedMessage.clearReactions();
  receivedMessage.channel.send(EMBED.CHAR_ERROR(CONFIG.MESSAGES.logsNotFound));
}

module.exports = {
  defaultError,
  commandNotfound,
  charNotfound,
  logsNotfound
}