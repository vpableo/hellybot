const getJobName = (job, soulCrystal) => {
  switch(job) {
    case 'gladiator / paladin':
      return soulCrystal !== null ? 'Paladin' : 'Gladiator';
    case 'marauder / warrior':
      return soulCrystal !== null ? 'Warrior' : 'Marauder';
    case 'dark knight / dark knight':
      return 'Dark Knight';
    case 'gunbreaker / gunbreaker':
      return 'Gunbreaker';
    case 'pugilist / monk':
      return soulCrystal !== null ? 'Monk' : 'Pugilist';
    case 'lancer / dragoon':
      return soulCrystal !== null ? 'Dragoon' : 'Lancer';
    case 'rogue / ninja':
      return soulCrystal !== null ? 'Ninja' : 'Rogue';
    case 'samurai / samurai':
      return 'Samurai';
    case 'conjurer / white mage':
      return soulCrystal !== null ? 'White Mage' : 'Conjurer';
    case 'arcanist / scholar':
      return soulCrystal !== null ? 'Scholar' : 'Arcanist';
    case 'astrologian / astrologian':
      return 'Astrologian';
    case 'archer / bard':
      return soulCrystal !== null ? 'Bard' : 'Archer';
    case 'machinist / machinist':
      return 'Machinist';
    case 'dancer / dancer':
      return 'Dancer';
    case 'thaumaturge / black mage':
      return soulCrystal !== null ? 'Black Mage' : 'Thaumaturge';
    case 'arcanist / summoner':
      return soulCrystal !== null ? 'Summoner' : 'Arcanist';
    case 'red mage / red mage':
      return 'Red Mage';
    case 'blue mage / blue mage':
      return 'Blue Mage';
    case 'carpenter / carpenter':
      return 'Carpenter';
    case 'blacksmith / blacksmith':
      return 'Blacksmith';
    case 'armorer / armorer':
      return 'Armorer';
    case 'goldsmith / goldsmith':
      return 'Goldsmith';
    case 'leatherworker / leatherworker':
      return 'Leatherworker';
    case 'weaver / weaver':
      return 'Weaver';
    case 'alchemist / alchemist':
      return 'Alchemist';
    case 'culinarian / culinarian':
      return 'Culinarian';
    case 'miner / miner':
      return 'Miner';
    case 'botanist / botanist':
      return 'Botanist';
    case 'fisher / fisher':
      return 'Fisher';
  }
}

module.exports = getJobName;