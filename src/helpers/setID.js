const setID = (args, receivedMessage) => {
  if (args.length === 0) return receivedMessage.author.id;
  const mentionID = args.toString();
  const strlength = mentionID.length;
  return mentionID.slice(3, strlength-1);
}

module.exports = setID
