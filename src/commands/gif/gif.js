const CONFIG = require('../../../config');
const EMBED = require('../../helpers/embed');

const GIPHY = require('giphy-api')(CONFIG.KEYS.giphy);

const gifCommand = (args, receivedMessage) => {
  if(args.length == 0){
    receivedMessage.channel.send(EMBED.ERROR(CONFIG.MESSAGES.commandError));
  } else {
    GIPHY.search({
      'q': args.toString().replace(',', '+'),
      'limit': CONFIG.LENGTH.gif,
    }).then((response) => {
      const itemsLength = response.data.length;
      const gif = response.data[Math.floor(Math.random()*itemsLength)].images.fixed_width.url;

      receivedMessage.channel.send(EMBED.GIF(gif));
    }).catch((err) => {
      return err
    })
  }
};

module.exports = gifCommand;