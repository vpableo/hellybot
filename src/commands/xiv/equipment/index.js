const request = require('request-promise');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;

const CONFIG = require('../../../../config');
// const EMBED = require('../../../helpers/embed');
const setId = require('../../../helpers/setID');
const errorMessages = require('../../../helpers/errorMessages');

const getItem = (id) => {
  let options = {
    uri: CONFIG.API.xiv + CONFIG.PATHS.item + id,
    qs: { private_key: CONFIG.KEYS.xiv },
    json: true
  }

  return new Promise((resolve, reject) => {
    // Do async job
    request.get(options, (error, response, body) => error ? reject(error) : resolve(body));
  })
}

var gearObject = {};

const mahequipCommand = (args, receivedMessage) => {
  receivedMessage.react('⏱');

  var userID = setId(args, receivedMessage);

  MongoClient.connect(CONFIG.DATASOURCE.db, (error, db) => {
    if (error) errorMessages.defaultError(receivedMessage);
    console.log("Database connected!");

    var dbo = db.db("hellybot");

    dbo.collection("character").find({'_id':userID}).toArray((error, arr) => {
      // more or less character not saved
      if (error) errorMessages.charNotfound(receivedMessage);
      if (arr.length > 0) {
        var gearSet = arr[0].profile.GearSet.Gear;

        for (var gearPart in gearSet) {
          if (gearSet.hasOwnProperty(gearPart)) {
            if (gearPart === 'SoulCrystal') {
              delete gearSet[gearPart]
            };
    
            let part = gearSet[gearPart];
    
            for (var spec in part) {
              if (part[spec] === null || part[spec] === undefined || spec === 'Creator') {
                delete gearSet[gearPart][spec];
              }
    
              if (spec === 'ID') {
                gearSet[gearPart].Name = part[spec];
              }
            }
          }
        }
    
        var gearSetTemp = gearSet;
    
        for (var part in gearSetTemp) {
          let gearPart = gearSetTemp[part];
    
          for(var gearSpec in gearPart) {
    
            if (gearSpec !== 'Materia' && gearSpec !== 'ID') {
              console.log(part + '->' + gearSpec);
              console.log(gearPart[gearSpec])
              let specPromise = getItem(gearPart[gearSpec]);
              specPromise.then((result) => {
                console.log(result.Name);
                gearObject[part][gearSpec] = result.Name;
              }, (error) => console.log(error)) 
            }
          }
        }
      } else
        errorMessages.charNotfound(receivedMessage);
    });
    
    console.log("Closing Database...");
    db.close();
  });
};

module.exports = mahequipCommand;
