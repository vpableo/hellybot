const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;

const CONFIG = require('../../../../config');
const EMBED = require('../../../helpers/embed');
const setID = require('../../../helpers/setID');
const errorMessages = require('../../../helpers/errorMessages');

const createEmbed = (result, receivedMessage) => {
  
  if (result){
    receivedMessage.clearReactions();
    receivedMessage.channel.send(EMBED.PORTRAIT(result.name, result.server, result.portrait));
  } else
    errorMessages.charNotfound(receivedMessage);
};

const mahpicCommand = (args, receivedMessage) => {
  var userID = setID(args, receivedMessage);

  receivedMessage.react('⏱');

  MongoClient.connect(CONFIG.DATASOURCE.db, (error, db) => {
    if (error) errorMessages.defaultError(receivedMessage);
    
    console.log("Database connected!");

    var dbo = db.db("hellybot");

    // gets same profile
    dbo.collection("character").find({'_id': userID}).toArray((error, arr) => {
      // more or less character not saved
      if (error) errorMessages.charNotfound(receivedMessage);
      createEmbed(arr[0], receivedMessage);
    });
    console.log("Closing Database...");
    db.close();
  });
};

module.exports = mahpicCommand;
