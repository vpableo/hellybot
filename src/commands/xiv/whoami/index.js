const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;

const CONFIG = require('../../../../config');
const EMBED = require('../../../helpers/embed');
const getJobName = require('../../../helpers/jobs');
const errorMessages = require('../../../helpers/errorMessages');

const createEmbed = (result, receivedMessage) => {
  if(result) {
    const character = result.profile;
    const freeCompany = result.freeCompany;
    const soulCrystal = character.GearSet.Gear.SoulCrystal;
    const activeJob = character.ActiveClassJob.Name;
    const activeJobLevel = character.ActiveClassJob.Level;
    const details = 'Lv' + activeJobLevel + ' ' + getJobName(activeJob, soulCrystal) + '\n' + freeCompany.Name + ' <' + freeCompany.Tag + '> of ' + freeCompany.Server +'\n'; 
    const embed = EMBED.CHAR_PROFILE(details, result.portrait, result.thumbnail, result.name, result.server)
    receivedMessage.clearReactions();
    receivedMessage.channel.send(embed);
  } else {
    receivedMessage.clearReactions();
    errorMessages.charNotfound(receivedMessage);
  }
};

const whoamiCommand = (command, args, receivedMessage) => {
  var userID;

  receivedMessage.react('⏱');

  if (command === 'whois' && args.length > 0) {
    const mentionID = args.toString();
    const strlength = mentionID.length;
    userID = mentionID.slice(3, strlength-1);
  } else {
    userID = receivedMessage.author.id;
  }

  MongoClient.connect(CONFIG.DATASOURCE.db, (error, db) => {
    if (error) errorMessages.defaultError(receivedMessage);
    console.log("Database connected!");

    var dbo = db.db("hellybot");

    // gets same profile
    dbo.collection("character").find({'_id':userID}).toArray((error, arr) => {
      // more or less character not saved
      if (error) errorMessages.charNotfound(receivedMessage);
      createEmbed(arr[0], receivedMessage)
    });

    console.log("Closing Database...");
    db.close();
  });
};

module.exports = whoamiCommand;
