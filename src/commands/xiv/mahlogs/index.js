const request = require('request-promise');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;

const CONFIG = require('../../../../config');
const EMBED = require('../../../helpers/embed')
const setID = require('../../../helpers/setID');
const getServerRegion = require('../../../helpers/getServerRegion')
const errorMessages = require('../../../helpers/errorMessages');

// Sets and saves into the logs collection
const setLogs = (result, receivedMessage, id) => {
  let region = getServerRegion(result.server);
  let name = result.name;
  let server = result.server;
  let dataCenter = result.profile.DC;
  let path = name + '/' + server + '/' + region
  let options = {
    uri: CONFIG.API.fflogs + CONFIG.PATHS.parses + path,
    qs: { api_key: CONFIG.KEYS.fflogs },
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true,
  }

  request(options)
    .then(response => {
      let fflog = {
        _id: id,
        name: name,
        server: server,
        region: region,
        dataCenter: dataCenter,
        profile: response,
      }

      MongoClient.connect(CONFIG.DATASOURCE.db, (err, db) => {
        if (err) errorMessages.defaultError(receivedMessage);
        console.log("Database connected!");

        var dbo = db.db("hellybot");

        // saves profile
        dbo.collection("logs").insertOne(fflog, (err, res) => {
          if (err) errorMessages.defaultError(receivedMessage);
          console.log("Documents inserted");
        });

        console.log("Closing Database...");
        db.close();
      });      

      receivedMessage.clearReactions();
      receivedMessage.channel.send(EMBED.ERROR(CONFIG.MESSAGES.logsSaved));

    })
    .catch(err => {
      console.log('error', err)
      errorMessages.logsNotfound(receivedMessage);
    });
}

//  main function
const logsCommand = (args, receivedMessage) => {
  receivedMessage.react('⏱')

  const id = setID(args, receivedMessage)

  MongoClient.connect(CONFIG.DATASOURCE.db, (error, db) => {
    if (error) {
      receivedMessage.clearReactions();
      errorMessages.defaultError(receivedMessage);
    }
    console.log("Database connected!");

    var dbo = db.db("hellybot");

    let resultCount = dbo.collection("logs").find({'_id':id}).count();

    if (resultCount > 0) {
      dbo.collection("logs").find({'_id':id}).toArray((error, arr) => {
        if (error) errorMessages.defaultError(receivedMessage);
        console.log("Results Found.");
        console.log(arr[0])
      })
    } else {
      dbo.collection("character").find({'_id':id}).toArray((error, arr) => {
        // more or less character not saved
        if (error || arr === null) errorMessages.charNotfound(receivedMessage);
        setLogs(arr[0],receivedMessage, id);
      });
    }

    console.log("Closing Database...");

    db.close();
  });
}
module.exports = logsCommand;
