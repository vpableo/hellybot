const request = require('request-promise');
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;

const CONFIG = require('../../../../config');
const EMBED = require('../../../helpers/embed');
const errorMessages = require('../../../helpers/errorMessages');

var characterSearchResult;
var characterProfile;

const getCharacter = (path, query) => {
  let queryString = query ? query : { private_key: CONFIG.KEYS.xiv }
  let options = {
    uri: CONFIG.API.xiv + path,
    qs: queryString,
    json: true
  }

  return new Promise((resolve, reject) => {
    // Do async job
    request.get(options, (error, response, body) => error ? reject(error) : resolve(body));
  })
}

const createResult = (result, receivedMessage) => {
  const name = result.name;
  const server = result.server;
  const charSaved = EMBED.SAVED_CHAR(name, server, result.thumbnail, CONFIG.MESSAGES.characterSaved)
  receivedMessage.clearReactions();
  receivedMessage.channel.send(charSaved);
};

const iamCommand = async (args, receivedMessage) => {
  receivedMessage.react('⏱');
  
  const userID = receivedMessage.author.id;
  const name = args[1] + ' ' + args[2];
  const server = args[0];
  
  let path = CONFIG.PATHS.characterSearch;
  let qs = {
    name: name,
    server: server,
    private_key: CONFIG.KEYS.xiv
  };
  
  var characterSearchPromise = getCharacter(path, qs);
  characterSearchPromise.then((result) => {
    characterSearchResult = result;
    console.log("Initialized user details");

    const id = characterSearchResult.Results[0].ID;
    const charPath = CONFIG.PATHS.character + id;
    let charDataQs = {
      data: 'FC',
      private_key: CONFIG.KEYS.xiv
    }

    // Gets the profile of the search result
    var characterProfilePromise = getCharacter(charPath, charDataQs);
    characterProfilePromise.then((result) => {
      characterProfile = result;

      const character = characterProfile.Character
      const entry = {
        _id: userID,
        name: character.Name,
        charID: character.ID,
        server: character.Server,
        thumbnail: character.Avatar,
        portrait: character.Portrait,
        profile: character,
        gear: {},
        gearProfile: character.GearSet.Gear,
        freeCompany: characterProfile.FreeCompany
      }

      MongoClient.connect(CONFIG.DATASOURCE.db, (err, db) => {
        if (err) errorMessages.defaultError(receivedMessage);
        console.log("Database connected!");

        var dbo = db.db("hellybot");

        // saves profile
        dbo.collection("character").insertOne(entry, (err, res) => {
          if (err) errorMessages.defaultError(receivedMessage);
          console.log("Documents inserted");
        });

        console.log("Closing Database...");
        db.close();
      });
      createResult(entry, receivedMessage);
    }, (error) => {
      errorMessages.charNotfound(receivedMessage)
    });
    // char not found
  }, (error) => {
    errorMessages.charNotfound(receivedMessage)
  });
};

module.exports = iamCommand;
