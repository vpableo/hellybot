const CONFIG = require('../../../config');
const EMBED = require('../../helpers/embed');

const wipCommand = (receivedMessage) => {
  receivedMessage.channel.send(EMBED.ERROR(CONFIG.MESSAGES.underDevelopment));
}

module.exports = wipCommand
