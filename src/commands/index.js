const errorMessages = require('../helpers/errorMessages');

const gifCommand = require('./gif/gif');
const iamCommand = require('./xiv/iam/index');
const whoamiCommand = require('./xiv/whoami/index');
const mahequipCommand = require('./xiv/equipment/index');
const mahpicCommand = require('./xiv/mahpic/index');
const helpCommand = require('./help/index');
const wipCommand = require('./underDevelopment/index');
const logsCommand = require('./xiv/mahlogs/index');

const processCommand = (receivedMessage) => {
  const fullCommand = receivedMessage.content.substr(1);
  const splitCommand = fullCommand.split(" ");
  const primaryCommand = splitCommand[0];
  const args = splitCommand.slice(1);

  // bestlogs = bestlogs <expansion> = gets the best logs for each expansion

  switch(primaryCommand) {
    case 'iam':
      iamCommand(args, receivedMessage);
      break;
    case 'whoami':
    case 'whois':
      whoamiCommand(primaryCommand, args, receivedMessage);
      break;
    case 'mahpic':
      mahpicCommand(args, receivedMessage);
      break;
    case 'mahgear':
    case 'eq':
      // wipCommand(receivedMessage);
      mahequipCommand(args, receivedMessage);
      break;
    case 'mahlogs':
      // wipCommand(receivedMessage);
      logsCommand(args, receivedMessage);
      break;
    case 'bestlogs':
      wipCommand(receivedMessage);
      // mahequipCommand(args, receivedMessage);
      break;    
    case 'gif':
      gifCommand(args, receivedMessage);
      break;
    case 'help':
      helpCommand(args, receivedMessage);
      break;
    default:
      errorMessages.commandNotfound(receivedMessage);
      break;
  }
};

module.exports = processCommand;
