
const CONFIG = require('../../../config');
const EMBED = require('../../helpers/embed');

const defaultHelpMessage = (receivedMessage) => {
  receivedMessage.channel.send(EMBED.HELP(CONFIG.MESSAGES.help));
}

const helpCommand = (args, receivedMessage) => {
  if (args && args.length > 0) {
    const command = args.toString();

    if (CONFIG.COMMAND.list.includes(command)) {
      for (var i=0; i < CONFIG.MESSAGES.help.length; i++) {
        if (CONFIG.MESSAGES.help[i].name === command) {
          receivedMessage.channel.send(EMBED.HELP2(CONFIG.MESSAGES.help[i]));
        }
      }
    } else {
      defaultHelpMessage(receivedMessage);
    }
  } else {
    defaultHelpMessage(receivedMessage);
  }
};

module.exports = helpCommand;