const Discord = require("discord.js");
const CONFIG = require('./config');

const processCommand = require('./src/commands/index');

const client = new Discord.Client();

client.on('ready', () => {
  console.log("Connected as " + client.user.tag);

  client.user.setActivity('luh-HEE', {type: 'LISTENING'});
});

client.on("message", (receivedMessage) => {
  if(receivedMessage.author == client.user) return;
  if(receivedMessage.content.startsWith(CONFIG.COMMAND.symbol)) processCommand(receivedMessage);
});

client.login(CONFIG.KEYS.discord);