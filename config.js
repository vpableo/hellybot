const KEYS = {
  discord: 'NTQ3OTMzNTUzMjU4NjU5ODQw.D09-dA.aVw-6iWtqkE3OvwKd20vnJRjelY',
  giphy: 'fOku3C9ZqmoIoqB8WZTDsJZHk51jcp4v',
  xiv: '31a7428fb36948cb8d015b31eef61057f18c690309014525bf9d4879f6dd6e81',
  fflogs: 'a72d0b8eac46a4b459e93523e6fcc3f8',
};

const API = {
  xiv: 'https://xivapi.com',
  fflogs: 'https://www.fflogs.com/v1/',
};

const PATHS = {
  character: '/character/',
  characterSearch: '/character/search',
  freeCompany: '/freecompany/',
  item: '/item/',
  parses: 'parses/character/',
}

const DATASOURCE = {
  // db: 'mongodb://localhost:27017/hellybot',
  db: 'mongodb+srv://admin:akZClpA5fPECphMB@hellybot-b20tt.mongodb.net/test?retryWrites=true&w=majority'
}

const COMMAND = {
  symbol: '/',
  list: [
    'iam', 'mahpic', 'gif', 'whoami', 'whois', 'mahgear', 'eq', 'mahlogs'
  ]
};

const LENGTH = {
  gif: 10,
};

const MESSAGES = {
  somethingWrong: '**There is something wrong with the request. Please try again later',
  underDevelopment: "Yo this command is probably under development. Don't be stupid, type `" + COMMAND.symbol + "help`",
  commandError: "Cannot process command. For more information, type `" + COMMAND.symbol + "help`",
  characterSaved: '**Character saved successfully!**\nYou can now use the following commands:\n\n```\n/whoami\n/mahgear\n/mahpic\n/mahlogs```\n\n',
  characterNotFound: '**Character not found!**\nSave your character first by using:\n\n```/iam <Server> <First Name> <Last Name>```\n\n',
  logsNotFound: '**Logs not found!**\nThere\'s probably something wrong with the request.',
  logsSaved: '**Your character\'s logs has been saved**\nHowever we\'re still working on how to display your logs at the meantime',
  help: [
    {
      name: 'iam',
      value: '**/iam <server> <firstname> <lastname>**\nRegisters character.\n'
    },
    {
      name: 'whoami',
      value: '**/whoami**\nGets your updated character profile'
    },
    {
      name: 'whois',
      value: '**/whois <mention user>**\nGets the updated profile of a saved character'
    },
    {
      name: 'mahpic',
      value: "**/mahpic <mention user -optional>**\nGets the your portrait or the mention user's portrait.\n",
    },
    {
      name: 'gif',
      value: '**/gif <keyword>**\nGets a random gif with the chose keyword.\n'
    },
  // mahequip or eq = gets current equipment and melds
  // mahlogs = get logs
  // bestlogs = bestlogs <expansion> = gets the best logs for each expansion
  ],
};

const SERVERS = {
  NA: [
    "Adamantoise",
    "Cactuar",
    "Faerie",
    "Gilgamesh",
    "Jenova",
    "Midgardsormr",
    "Sargatanas",
    "Siren",
    "Behemoth",
    "Excalibur",
    "Exodus",
    "Famfrit",
    "Hyperion",
    "Lamia",
    "Leviathan",
    "Ultros",
    "Balmung",
    "Brynhildr",
    "Coeurl",
    "Diabolos",
    "Goblin",
    "Malboro",
    "Mateus",
    "Zalera"
  ],
  JP: [
    "Aegis",
    "Atomos",
    "Carbuncle",
    "Garuda",
    "Gungnir",
    "Kujata",
    "Ramuh",
    "Tonberry",
    "Typhon",
    "Unicorn",
    "Alexander",
    "Bahamut",
    "Durandal",
    "Fenrir",
    "Ifrit",
    "Ridill",
    "Tiamat",
    "Ultima",
    "Valefor",
    "Yojimbo",
    "Zeromus",
    "Anima",
    "Asura",
    "Belias",
    "Chocobo",
    "Hades",
    "Ixion",
    "Mandragora",
    "Masamune",
    "Pandaemonium",
    "Shinryu",
    "Titan"
  ],
  EU: [
    "Cerberus",
    "Louisoix",
    "Moogle",
    "Omega",
    "Ragnarok",
    "Spriggan",
    "Lich",
    "Odin",
    "Phoenix",
    "Shiva",
    "Zodiark",
    "Twintania"  
  ],
};

module.exports = {
  KEYS,
  API,
  LENGTH,
  MESSAGES,
  COMMAND,
  PATHS,
  DATASOURCE,
  SERVERS,
};